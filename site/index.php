<?php
/**
 * @package     BreezingCommerce
 * @author      Markus Bopp
 * @link        http://www.crosstec.de
 * @license     GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');

$libpath = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_breezingcommerce' . DS . 'classes' . DS . 'plugin' . DS;

$mercadopagoDir = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'squarecheckout' . DS . 'vendor' . DS;

$mercadopagoDirSite = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'squarecheckout' . DS;

require_once($libpath . 'CrBcAPaymentSitePlugin.php');
require_once($libpath . 'CrBcPaymentSitePlugin.php');
require_once($mercadopagoDir . 'autoload.php');
class CrBc_Plugins_Payment_Squarecheckout_Site extends CrBcAPaymentSitePlugin implements CrBcPaymentSitePlugin
{

  public function __construct(){
  	parent::__construct();
    $this->table = '#__breezingcommerce_plugin_payment_squarecheckout';
  }

  public function getPluginIcon(){

    $plugin_path = 'media/breezingcommerce/plugins/payment/mercadopago/';

    $document = JFactory::getDocument();

    $document->addStylesheet( $plugin_path . 'site/node_modules/font-awesome/css/font-awesome.min.css' );

    return '<i class="fa fa-money" aria-hidden="true"></i>';
  }

  function getPaymentInfo(){

  }

  public function getInitOutput(){

    parent::__construct();

    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'CrBcCart.php');

    $_session_cart = JFactory::getSession()->get('crbc_cart', array());

    $db = JFactory::getDbo();

    if(!isset($_session_cart['checkout']) || !isset($_session_cart['checkout']['payment_plugin_id'])){
      throw new Exception('User checkout not performed yet');
    }

    $payment_plugin_id = intval($_session_cart['checkout']['payment_plugin_id']);

    $_cart = new CrBcCart( $_session_cart );
    $_cart_items = $_cart->getItems(true);

    if(count($_cart_items) == 0){

        throw new Exception('Trying to pay an empty cart');
    }

    $db->setQuery("Select * From #__breezingcommerce_plugins Where published = 1 And type = 'shipping' Order By `ordering`");
    $shipping_plugins = $db->loadAssocList();

    $data = CrBcCart::getData($_session_cart['order_id'], $_cart_items, -1, -1);

    $_order_info = CrBcCart::getOrder(
                                $_session_cart['order_id'],
                                $_cart,
                                $_session_cart,
                                $_cart_items,
                                $_session_cart['customer_id'],
                                $data,
                                $shipping_plugins,
                                array()
                        );

    if($_order_info->grand_total <= 0){

        throw new Exception('Trying to use payment local while the total is zero.');
    }

    $dirPath = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'squarecheckout' . DS;

    $plugin_path = 'media/breezingcommerce/plugins/payment/squarecheckout/';

    $document = JFactory::getDocument();
    $document->addScript('https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js');
    $document->addScript('https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js');

    $db = JFactory::getDbo();

    $db->setQuery("SELECT * FROM ".$this->table." limit 1");

    $sq = $db->loadObject();

    if(!($sq instanceof stdClass)){
      throw new Exception('No SQUARE settings');
    }

    $sq->items             = $_cart_items;

    $sq->order_id          = $_session_cart['order_id'];

    $sq->no_shipping       = $_cart->isVirtualOrder($_cart_items) ? 1 : 0;

    $sq->shipping          = $_order_info->shipping_costs;

    $sq->payment_plugin_id = $payment_plugin_id;

    $sq->urlreturn = JUri::getInstance()->toString() . "&verify_payment=1&payment_plugin_id=" . $sq->payment_plugin_id . "&order_id=" . $sq->order_id ;



    ob_start();
    //require_once( $dirPath . DS . 'site' . DS . 'main.js.php';
    require_once( JPATH_SITE . '/media/breezingcommerce/plugins/payment/squarecheckout/site/tmpl/payment.php' );
    $content = ob_get_contents();
    ob_end_clean();

    return $content;
  }

  public function verifyPayment(CrBcCart $_cart, stdClass $order){
    $checkoutId = JRequest::getVar("checkoutId", null);
    $transactionId = JRequest::getVar("transactionId", null);

    if(is_null($checkoutId) || is_null($transactionId)){
      return false;
    }

    return true;
  }

  public function getPaymentTransactionId(){

  }

  function getAfterPaymentInfo(){
    return JText::_('PAY WITH SQUARE');
  }

  public function getPluginDisplayName(){

    return 'SQUARE PAYMENT';
  }

  public function getPluginDescription(){
      return 'Payment with creditcard';
  }
}
