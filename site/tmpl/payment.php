<script type="application/json" id="jsoncartitems">
<?php
echo json_encode($sq->items);
?>
</script>
<script>
let squarecheckout_return = "<?php echo $sq->urlreturn; ?>";
</script>
<div id="squarecheckout">
  <div class="uk-card uk-card-body uk-width-1-2 uk-align-center">
    <span uk-spinner="ratio: 4"></span> Redirecting...
  </div>
</div>
<script src='<?php echo "/" . $plugin_path . "site/tmpl/squarecheckout.js"; ?>'></script>
