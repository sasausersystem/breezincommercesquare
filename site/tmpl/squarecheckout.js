const squarecheckout = new Vue({
  el: '#squarecheckout',
  data(){
    return {
      uri: '/media/breezingcommerce/plugins/payment/squarecheckout/site/checkout.php',
      stateLoading: 0,
      cartitem: [],
      message: 'Hello'
    };
  },
  mounted(){
    this.getItems();
    setTimeout(()=>{
      this.gotopay();
    }, 800);
  },
  methods: {
    getItems(){
      let jsoncontainer = document.getElementById("jsoncartitems");
      console.log();
      this.cartitem = JSON.parse(jsoncontainer.innerHTML);
    },
    gotopay(){
      let data = new FormData();
      data.append("items", JSON.stringify(this.cartitem));
      data.append("urireturn", squarecheckout_return);
      axios.post(this.uri+"?opt=GetLink", data).then((resp)=>{
        if(resp.data.error == null){
          setTimeout(()=>{
            window.location.href=resp.data.data;
          }, 800);
        }
      }).catch((err) => {
        console.log(err);
      });
    }
  }
});
