<?php

define( 'DS', DIRECTORY_SEPARATOR );

if(!defined('JPATH_BASE')){
  define('JPATH_BASE', dirname( dirname( dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) ) ) ));
}

define('_JEXEC', true);

include( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );

require_once( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );
require_once( JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'database' . DS . 'factory.php' );

function convertToCents($amount){
  $result = intval( strval(floatval( preg_replace("/[^0-9.]/", "", str_replace(',','.',$amount) ) ) * 100));
  return $result;
}

$mercadopagoDir = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'squarecheckout' . DS . 'vendor' . DS;

require_once $mercadopagoDir . 'autoload.php';

use Square\Models\CreateOrderRequest;
use Square\Models\CreateCheckoutRequest;
use Square\Models\Order;
use Square\Models\OrderLineItem;
use Square\Models\Money;
use Square\Exceptions\ApiException;
use Square\SquareClient;

$response = new stdClass();

JFactory::getApplication('site');

$db = JFactory::getDbo();

$config = JFactory::getConfig();

$mp_session = JFactory::getSession();

$mp_session->start();

$db->setQuery("SELECT * FROM #__breezingcommerce_plugin_payment_squarecheckout order by identity desc limit 1");

$sq = $db->loadObject();


$enviroment = ($sq->mode == '0') ? 'sandbox' : 'production';
$accesstoken = $sq->access_token;
$appid = $sq->app_id;
$locationid = $sq->location_id;

switch ($_GET['opt']){
  case 'GetLink':
    header('Content-Type: application/json');
    // Initialize the authorization for Square
    $client = new SquareClient([
      'accessToken' => $accesstoken,
      'environment' => $enviroment,
    ]);

    try {

      $items = JRequest::getVar('items', null);
      if(is_null($items)){
        $response->error = 1;
        echo json_encode($response);
        exit();
      }

      $jsonItems = json_decode($items);
      if(!is_array($jsonItems)){
        $response->error = 1;
        echo json_encode($response);
        exit();

      }

      $checkout_api = $client->getCheckoutApi();
      $currency = $client->getLocationsApi()->retrieveLocation($locationid)->getResult()->getLocation()->getCurrency();

      $itemsToOrder = array();

      foreach ($jsonItems as $key => $value) {
        $cents = convertToCents($value->price_gross);
        $money = new Money();
        $money->setCurrency($currency);
        $money->setAmount($cents);

        $it = new OrderLineItem($value->amount);
        $it->setName($value->title);
        $it->setBasePriceMoney($money);

        array_push($itemsToOrder, $it);
      }

      $order = new Order($locationid);
      $order->setLineItems($itemsToOrder);

      $createOrder = new CreateOrderRequest($order);
      $createOrder->setOrder($order);

      $checkout = new CreateCheckoutRequest(uniqid(), $createOrder);

      $uriReturn = JRequest::getVar("urireturn", null);

      $checkout->setRedirectUrl($uriReturn);

      $resp = $checkout_api->createCheckout($locationid, $checkout);

      $link = $resp->getResult()->getCheckout()->getCheckoutPageUrl();

      $response->error = null;
      $response->data = $link;
      echo json_encode($response);
      exit();
    } catch(Exception $e) {
      $response->error = $e->getMessage();
      echo json_encode($response);
      exit();
    }
  break;
}

?>
