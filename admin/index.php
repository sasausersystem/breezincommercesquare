<?php
/**
 * @package     BreezingCommerce
 * @author      Jesus Uzcategui
 * @link        http://www.crosstec.de
 * @license     GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');

$libpath = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_breezingcommerce' . DS . 'classes' . DS . 'plugin' . DS;
require_once($libpath . 'CrBcAPaymentAdminPlugin.php');
require_once($libpath . 'CrBcPaymentAdminPlugin.php');

class CrBc_Plugins_Payment_Squarecheckout_Admin extends CrBcAPaymentAdminPlugin implements CrBcPaymentAdminPlugin {
  public function  __construct() {

      require_once(JPATH_SITE.'/administrator/components/com_breezingcommerce/classes/CrBcPane.php');

      // always call the parent constructor and always call it _first_
      parent::__construct();

      // define the default table for built-in list/details view
      $this->table = '#__breezingcommerce_plugin_payment_squarecheckout';

  }

  public function afterStore($data){}

  function display(){
    $this->setDetailsView(array('apply', 'cancel'));
  }

  function setDetailsView($toolbarItems = array()){

    $document = JFactory::getDocument();
    $document->addStylesheet('https://cdn.jsdelivr.net/npm/siimple@3.3.1/dist/siimple.min.css');

    $this->setToolbar($toolbarItems);
    $this->template = 'detailts';

    $db = JFactory::getDbo();

    $db->setQuery("SELECT * FROM " . $this->table . " ORDER BY ". $this->identity_column ."  DESC LIMIT 1");

    $row = $db->loadObject();

    if(!($row instanceof stdClass)){
        $row = new stdClass();
        $id = $this->identity_column;
        $row->$id = 0;
        $row->mode = 0;
        $row->app_id = '';
        $row->access_token = '';
        $row->location_id = '';
    }

    $this->assignRef('entity', $row);

  }

  function init($subject = null){

  }

  function getPaymentInfo(){

  }

  function getAfterPaymentInfo(){
    return JText::_('PAYD WITH SQUARE');
  }

  public function getPluginDisplayName(){
    return JText::_('SQUARE');
  }

  public function getPluginDescription(){
    return JText::_('PAYMENT WITH SQUARE');
  }
}
