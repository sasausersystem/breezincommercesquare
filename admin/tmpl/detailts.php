<?php
?>
<script></script>
<style>
*{
  line-height: normal !important;
}
</style>
<div id="SquareSettings">
  <div class="siimple-jumbotron">
      <div class="siimple-jumbotron-title">SQUARE FOR BreezingCommerce</div>
      <div class="siimple-jumbotron-subtitle">Set your configurations</div>
  </div>
  <div class="siimple-form">
      <div class="siimple-form-field">
        <label class="siimple-label">MODE PRODUCTION:
        <div class="siimple-radio">
            <input type="radio" id="mode1" name="mode" value="1" <?php echo ( intval($this->entity->mode) == 1) ? 'checked="checked"' : '' ?>>
            <label for="mode1"></label>
        </div>
      </div>
      <div class="siimple-form-field">
        <label class="siimple-label">MODE SANBOX:
        <div class="siimple-radio">
            <input type="radio" id="mode0" name="mode" value="0" <?php echo ( intval($this->entity->mode) == 0) ? 'checked="checked"' : '' ?>>
            <label for="mode0"></label>
        </div>
      </div>
      <div class="siimple-form-field">
          <div class="siimple-form-field-label">APP ID</div>
          <input type="text" class="siimple-input siimple-input--fluid" placeholder="PUT YOUR APP ID" name="app_id" value="<?php echo $this->entity->app_id; ?>">
      </div>
      <div class="siimple-form-field">
          <div class="siimple-form-field-label">ACCESS TOKEN</div>
          <input type="text" class="siimple-input siimple-input--fluid" placeholder="PUT YOUR ACCESS TOKEN" name="access_token" value="<?php echo $this->entity->access_token; ?>">
      </div>
      <div class="siimple-form-field">
          <div class="siimple-form-field-label">LOCATION ID</div>
          <input type="text" class="siimple-input siimple-input--fluid" placeholder="PUT YOUR LOCATION ID" name="location_id" value="<?php echo $this->entity->location_id; ?>">
      </div>
  </div>
</div>
