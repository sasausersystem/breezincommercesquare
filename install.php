<?php

defined('_JEXEC') or die('ACCESS DENIED');

class CrBcInstallation extends CrBcInstaller {
    public $type = 'payment';
    public $name = 'squarecheckout';

    function install(){
        $query = <<<SQL
        CREATE TABLE #__breezingcommerce_plugin_payment_squarecheckout(
            `identity` int(11) not null primary key auto_increment,
            `mode` int(3) default 0,
            `app_id` text null,
            `access_token` text null,
            `location_id` text null,
        );
        SQL;
        $db = JFactory::getDbo();
        $db->setQuery($query);
        $db->query();
    }
}
